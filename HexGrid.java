public class MainActivity extends AppCompatActivity {

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        final int BUTTON_COUNT = 96;
        final int BUTTON_SIZE = 200;

        final int COLUMNS = 3;

        ArrayList<View> buttons = new ArrayList<>();

        for (int i = 0; i < BUTTON_COUNT; i++) {
            // Define teh buttons
            Button button = new Button(this);

            button.setMinHeight(0);
            button.setMinWidth(0);

            button.setWidth(BUTTON_SIZE);
            button.setHeight(BUTTON_SIZE);

            button.setText("Quiz\nName #" + String.valueOf(i + 1));

            button.setBackgroundResource(R.drawable.hexagon);

            buttons.add(button);
        }

        FrameLayout rootView = (FrameLayout) findViewById(R.id.main_frame);

        rootView.addView(createHexGrid(buttons, COLUMNS, BUTTON_SIZE));
    }

    public LinearLayout createHexGrid (ArrayList<View> views, int columns, int viewSize) {
        // The following must be calculated using the hexagon's radius
        // But ain't nobody got time for dat.
        final int HORIZONTAL_MARGIN = 20;
        final int VERTICAL_MARGIN = 20;

        // Buttons are wrapped in FrameLayouts
        ArrayList<FrameLayout> buttonsContainers = new ArrayList<>();

        for (int i = 0; i < views.size(); i++) {
            // Pack the buttons in containers
            FrameLayout container = new FrameLayout(this);
            container.setPadding(HORIZONTAL_MARGIN/2, 0, HORIZONTAL_MARGIN/2, 0);

            container.addView(views.get(i));

            buttonsContainers.add(container);
        }

        // fill the horizontal layouts with buttons
        ArrayList<LinearLayout> horizontalLayouts = new ArrayList<>();
        ArrayList<FrameLayout> consumableContainers =
                (ArrayList<FrameLayout>) buttonsContainers.clone();

        while (consumableContainers.size() > 0) {

            // Define a horizontal layout
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.HORIZONTAL);

            // Give it default layout params
            ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            );

            // The number of columns alternates between MAX COLS and MAX COLS - 1
            int cols = columns - horizontalLayouts.size() % 2;

            // The left padding depends on the row we're in
            layout.setPadding(
                    // The following line is ZERO if size is even
                    horizontalLayouts.size() % 2 * (viewSize+HORIZONTAL_MARGIN) / 2,
                    -VERTICAL_MARGIN,
                    0,
                    -VERTICAL_MARGIN
            );

            layout.setLayoutParams(layoutParams);

            for (int j = 0; j < cols; j++) {
                if (consumableContainers.size() > 0) {
                    layout.addView(consumableContainers.get(0));
                    consumableContainers.remove(0);
                }
            }

            horizontalLayouts.add(layout);
        }

        // Remove top padding from the first horizontal layout
        LinearLayout firstLayout = horizontalLayouts.get(0);
        firstLayout.setPadding(
                firstLayout.getPaddingLeft(),
                0,
                firstLayout.getPaddingRight(),
                firstLayout.getPaddingBottom()
        );

        // Remove bottom padding from the last horizontal layout
        LinearLayout lastLayout = horizontalLayouts.get(horizontalLayouts.size() - 1);
        lastLayout.setPadding(
                lastLayout.getPaddingLeft(),
                lastLayout.getPaddingTop(),
                lastLayout.getPaddingRight(),
                0
        );

        // Define the main vertical layout
        LinearLayout verticalLayout = new LinearLayout(this);
        verticalLayout.setOrientation(LinearLayout.VERTICAL);

        // The next line does MAGIC
        verticalLayout.setClipChildren(false);

        // Add the horizontal layouts to the main vertical layout
        for (LinearLayout horizontalLayout : horizontalLayouts) {

            verticalLayout.addView(horizontalLayout);
        }

        return verticalLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}